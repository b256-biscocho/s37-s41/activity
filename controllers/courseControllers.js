const Course = require("../models/Course.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
// Create a new course
/*
	Business Logic:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new Course to the database
*/
module.exports.addCourse = (course) => {
	
	let newCourse = new Course({
		name: course.name,
		description: course.description,
		price:course.price
	});

	return newCourse.save().then((result, err) => {

		if(err) {

			return false;

		} else {

			return true;
			
		}
	})
}

//retrieving ALL courses
module.exports.getAllCourses = () =>{
	return Course.find({}).then(result => {
		return result;
	})
}

//Retrieving all active courses
module.exports.getActiveCourses = () =>{
	return Course.find({isActive: true}).then(result => {
		return result;
	})
}


//Retrieving a specific course
/*
	Business Logic:
	1. Retrieve the course that matches the course ID provided from the URL
		reqParams = req.params(url) = "/:courseId"
*/
module.exports.getCourse = (reqParams) => {
		return Course.findById(reqParams.courseId).then(result => {
		return result;
})
}

//Updating a course
/*
	Business Logic:
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
	2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
*/
//course = data.course =req.body
//paramsId = data.params = req.params = /:courseId
module.exports.updateCourse = (course, paramsId) => {
	//specify the field/properties that will be updated
	//whats on create should be the same as update
	let updatedCourse = {
		name: course.name,
		description: course.description,
		price: course.price
	}
	//findByIdAndUpdate(id, updatedContent)
	return Course.findByIdAndUpdate(paramsId.courseId, updatedCourse).then((result, err)=>{ 
		if(err) {
			return false;
		} else {
			return true;
	}
})
}
/*
//Activity40
module.exports.archiveCourse = (course, paramsId) => {
	//specify the field/properties that will be updated
	//whats on create should be the same as update
	let archiveCourse = {
		name: course.name,
		description: course.description,
		price: course.price
	}
	//findByIdAndUpdate(id, archiveCourse)
	return Course.findByIdAndUpdate(paramsId.courseId, archiveCourse).then((result, err)=>{
		if(err) {
			return false;
		} else {
			return true;
	}
})
}
*/

/*
// Route to archiving a course
// A "PUT" request is used instead of "DELETE" request because of our approach in archiving and hiding the courses from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases
router.put("/archive/:courseId", auth.verify, (req, res) => {

	const data ={
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}

	if(data.isAdmin) {

		courseController.archiveCourse(data.params).then(resultFromController => res.send(resultFromController));
	
	} else {

		res.send(false);

	}
});

module.exports = router;

*/


// Archive a course
// In managing databases, it's common practice to soft delete our records and what we would implement in the "delete" operation of our application
// The "soft delete" happens here by simply updating the course "isActive" status into "false" which will no longer be displayed in the frontend application whenever all active courses are retrieved
// This allows us access to these records for future use and hides them away from users in our frontend application
// There are instances where hard deleting records is required to maintain the records and clean our databases
// The use of "hard delete" refers to removing records from our database permanently
module.exports.archiveCourse = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {

		// Course not archived
		if (error) {

			return false;

		// Course archived successfully
		} else {

			return true;

		}

	});
};
