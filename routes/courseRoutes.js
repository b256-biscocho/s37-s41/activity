const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers.js");
const auth = require("../auth.js");
/*// Route for creating a course
router.post("/create", (req, res) => {

	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));

});
*/
//ACTIVITY
// Route for creating a course
router.post("/create", auth.verify, (req, res) => {

	// Check if the user making the request is an admin

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	} 

	if(data.isAdmin) {

		courseController.addCourse(data.course).then(resultFromController => res.send(resultFromController));


	} else {
		res.send(false);
	}
});




//Route for retrieving ALL courses
router.get("/all", (req,res)=>{

	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));

})


//Route for retrieving all active courses
router.get("/active", (req,res)=>{
	courseController.getActiveCourses().then(resultFromController => res.send(resultFromController));
})


//Route for retrieving specific course
router.get("/:courseId", (req,res)=>{
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})


router.put("/update/:courseId", auth.verify, (req, res)=>{
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}
	if (data.isAdmin){

	courseController.updateCourse(data.course, data.params).then(resultFromController => res.send(resultFromController));	
	} else{
		res.send(false);
	}
})
/*

//Activity 40
/*  Create a route for archiving a course. The route must use JWT authentication and obtain the course ID from the url.
    Create a controller method for archiving a course obtaining the course ID from the request params and the course information from the request body.
 
router.patch("/archive/:courseId", auth.verify, (req, res)=>{
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}
	if (data.isAdmin){

	courseController.archiveCourse(data.course, data.params).then(resultFromController => res.send(resultFromController));	
	} else{
		res.send(false);
	}
})
*/


// Route to archiving a course
// A "PUT" request is used instead of "DELETE" request because of our approach in archiving and hiding the courses from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases
router.put("/archive/:courseId", auth.verify, (req, res) => {

	const data ={
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}

	if(data.isAdmin) {

		courseController.archiveCourse(data.params).then(resultFromController => res.send(resultFromController));
	
	} else {

		res.send(false);

	}
});

module.exports = router;


module.exports = router;