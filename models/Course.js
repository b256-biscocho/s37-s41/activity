const mongoose = require("mongoose");

//create a schema 
const courseSchema = new mongoose.Schema({
	//field: data_type
	name: {
		type: String,
		//Requires the data for this field to be included when creating a record
		//the true calue defines if the field is required or not
		//the second element in the array is the message that will display in the console when the data is not present
		required: [true, "Course Name is required"]
	},
	description: {
		type: String,
		required: [true, "Course Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,

		//new Date( is an expression that instantiate a new date that stores the current date and time whenever the coruse is created in our database)
		default: new Date()
	},
	//the entollees property will be an array of objects containing ussedID and date and time that the user enrolled
	enrollees: [
	{
		userId: {
			type: String,
			required: [true, "User ID is required"]
		},
		enrolledOn: {
			type: Date,
			default: new Date()
		}
	}]

});

//model -> Couse
//Collections -> courses
module.exports = mongoose.model("Course", courseSchema)
